package jmarc.modeling;

public interface MARCOptimizationProblem {

    double objectiveFunction(MARCSolution sol);

}
