package jmarc.modeling;

public interface MARCHeuristicProblem {

    double heuristic(MARCState s);

}
