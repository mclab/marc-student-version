package jmarc.modeling;

public interface MARCRandomProblem {

    MARCState getRandomState();

}
