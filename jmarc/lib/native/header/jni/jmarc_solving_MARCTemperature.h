/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class jmarc_solving_MARCTemperature */

#ifndef _Included_jmarc_solving_MARCTemperature
#define _Included_jmarc_solving_MARCTemperature
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     jmarc_solving_MARCTemperature
 * Method:    createNativeObject
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_jmarc_solving_MARCTemperature_createNativeObject
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
